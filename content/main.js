(function () {
  "use strict";

  /*
  This function tries to use the standard HTML storage API if possible, falling
  back to an emulated version if something goes wrong. This can happen when the
  page is being viewed offline as a `file://` URI, since the storage APIs don't
  work properly in this case (or, at least, not in all browsers).
  */

  function initStorage(completeFunc) {
    try {
      // Try to use the standard HTML5 storage API:
      sessionStorage.getItem("test");
      // If that worked, then everything is OK and we can proceed as normal:
      completeFunc(sessionStorage, true);
      return;
    } catch (e) { /* A "SecurityError" prevents access for `file://` URIs. */ }

    // Emulate the usual HTML5 storage API:
    window.pseudoStorage = {
      setItem: function (key, value) {
        window.pseudoStorage.data[key] = value;
        // If we're running within a frame, ask the parent to remember this:
        if (top !== self) { window.parent.postMessage(["setVar", key, value], "*"); }
      },
      getItem: function (key) {
        return window.pseudoStorage.data[key];
      },
      removeItem: function (key) {
        window.pseudoStorage.setItem(key, undefined);
      },
      clear: function () {
        window.pseudoStorage.data = {};
        // If we're running within a frame, ask the parent to remember this:
        if (top !== self) { window.parent.postMessage(["delete"], "*"); }
      },
      data: {}
    };

    // If we're running within a frame, try to restore the old values:
    if (top !== self) {
      var restoreStorage = function(event) {
        window.removeEventListener("message", restoreStorage, false);
        window.pseudoStorage.data = event.data;
        completeFunc(window.pseudoStorage, true);
      }
      // Send a "restore" request to the parent frame, and wait for a response:
      window.addEventListener("message", restoreStorage, false);
      window.parent.postMessage(["getAll"], "*");
    } else {
      // If there is no parent frame, then we have nowhere to restore from:
      completeFunc(window.pseudoStorage, false);
    }
  }

  // Attempt to initialise the storage API when the page first loads:
  window.addEventListener("load", function () {
    initStorage(function (api, persisted) {
      // Try to read a key that may or may not exist yet:
      var test = api.getItem("hello");
      if (test) {
        // If it exists, then it's been remembered from last time:
        console.log("The key exists: It's", test)
        api.clear();
      } else if (persisted) {
        // If it doesn't, but will be remembered, store it for next time:
        api.setItem("hello", "world");
        console.log("The key doesn't exist; added...")
      } else {
        // If it doesn't exist, and won't be remembered, then just give up:
        console.log("Session storage is not available.")
      }

    });
  });

})();
