// Create an object to store things, as per localStorage/sessionStorage:
window.pseudoSession = {};

// Listen for any messages sent by the iframe:
window.addEventListener("message", function (event) {
  switch (event.data[0]) {
    case "delete": window.pseudoSession = {}; break;
    case "setVar": window.pseudoSession[ event.data[1] ] = event.data[2]; break;
    case "getAll": event.source.postMessage(window.pseudoSession, "*"); break;
  }
}, false);
