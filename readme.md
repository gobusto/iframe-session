Emulating sessionStorage with an iframe
=======================================

This is a proof-of-concept implementation, intended to demonstrate how the HTML
[`sessionStorage`](https://developer.mozilla.org/en-US/docs/Web/API/Window/sessionStorage)
API can be emulated by using a "container" page (with an `iframe` element), for
situations where the real `sessionStorage` API is not an option.

The Problem
-----------

I wanted to store session data for multiple "offline" HTML pages, accessed with
the [`file://`](https://en.wikipedia.org/wiki/File_URI_scheme) URI scheme. This
is surprisingly difficult to do with the usual storage mechanisms:

### Cookies

Most browsers don't allow cookies for pages accessed via a `file://` URI (or at
least not by default); see <https://bugs.chromium.org/p/chromium/issues/detail?id=535>

### localStorage and sessionStorage

As with cookies, most browsers don't allow storage to be used on pages accessed
via a `file://` URI, raising a `SecurityError` if scripts attempt to use them.

### File System API

At time of writing, [very few browsers](http://caniuse.com/#search=FileSystem)
support the File System API - not that it matters too much, since (once again)
[`file://` URIs would not work](https://developer.mozilla.org/en-US/docs/Web/API/File_System_API/Introduction#file)
anyway.

### GET parameters

The current "session" could be passed from one page to another by intercepting
the click events for `<a>` tags and appending any important values to the path
they specify: `file:///home/thomas/index.html?sessionvar1=abc&sessionvar2=xyz`

Whilst this method could work, there are a few potential problems:

+ The maximum size allowed for a GET request is [fairly small](http://stackoverflow.com/a/417184/4200092).
+ It might be tricky to transfer anything more complex than a string or number.
+ The values are plainly visible, making it easy for users to change them.

The Solution
------------

My solution: Display the "real" page in an `<iframe>` tag and use a "container"
page to store any session values. This way, the data can be "read back" by the
new "child" page after it changes; here's a simplified pseudocode example:
    
    # Get a previously-stored variable:
    def get_variable(key)
      return session[key]
    end
    
    # Set a variable, and store a "backup" copy on the container page:
    def set_variable(key, value)
      session[key] = value
      container.session[key] = value
    end
    
    # Copy the data from the "container" page, putting us back where we were:
    def on_page_load()
      session = container.session
    end

The actual Javascript code is of course slightly more complex:

+ The `sessionStorage` API is used if possible, with the "container-as-storage"
  method only being used as a fallback for situations where it isn't available.
+ Communication between the parent window and the iframe is subject to security
  restrictions, requiring [`postMessage()`](https://developer.mozilla.org/en-US/docs/Web/API/Window/postMessage)
  to be used (as opposed to simply having the iframe directly read/write values
  on the parent `window` object).
+ The Javascript code on the "child" page checks whether or not it actually has
  a "container" page, so it won't cause errors if the page is viewed "directly"
  (i.e. outside of an iframe).

This is a proof-of-concept implementation - it has been tested and confirmed to
work as expected in both Firefox 45 and Chrome 51, though other browsers should
also work without modifications.

Limitations
-----------

If a link on the child page is opened in a new tab, the contents of the new tab
will not have access to the "container" page session data. This is actually how
the "real" `sessionStorage` API behaves, so it's not too much of a problem:

> ["Opening a page in a new tab or window will cause a new session to be initiated" - MDN](https://developer.mozilla.org/en-US/docs/Web/API/Window/sessionStorage)

However, the fact that the "real" page is actually running in an `iframe` means
that some things - such as the browser back/forward/reload buttons - won't work
as expected, and any `<title>` text on the child page will be ignored.

Miscellaneous Notes
-------------------

The code for the child page originally used `frameElement` to determine whether
or not it was running in an iframe. This works in Firefox, but not in Chromium:
`null` is returned even within an iframe, so `top !== self` is used instead.

License
-------

Copyright (c) 2016 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
